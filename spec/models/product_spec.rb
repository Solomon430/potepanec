require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon, name: "Bags") }
  let(:product) { create(:product, name: "Bag", taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 20, taxons: [taxon]) }

  it "chenges the order of related products when reloading the page" do
    related_products_before = Spree::Product.without_myself(product).related_products(product, 4)
    related_products_after = Spree::Product.without_myself(product).related_products(product, 4)
    expect(related_products_before == related_products_after).to be_falsy
  end

  it "squeezed related products to 4" do
    expect(related_products.count).to eq 20
    related_products_after = Spree::Product.without_myself(product).related_products(product, 4)
    expect(related_products_after.count).to eq 4
  end
end
