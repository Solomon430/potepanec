require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxonomies) { create_list(:taxonomy, 2) }
  let!(:taxonomy) { taxonomies.first }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:option_types) do
    [
      create(:option_type, presentation: "Color"),
      create(:option_type, presentation: "Size"),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: "Red", option_type: option_types[0]),
      create(:option_value, name: "Blue", option_type: option_types[0]),
      create(:option_value, name: "Black", option_type: option_types[0]),
      create(:option_value, name: "White", option_type: option_types[0]),
    ]
  end

  describe "GET #show" do
    let!(:products) { create_list(:product, 3, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "resoponds successfully" do
      expect(response).to be_successful
    end

    it "render show template" do
      expect(response).to render_template :show
    end

    it "has correct taxonomies" do
      expect(assigns(:taxonomies)).to match_array taxonomies
    end

    it "has correct taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "has correct products" do
      expect(assigns(:products)).to match_array products
    end

    it "has correct colors" do
      expect(assigns(:colors)).to match_array colors
    end
  end

  describe "GET #sort" do
    context "order_by_price" do
      let!(:products) do
        [
          create(:product, taxons: [taxon], price: 10.00),
          create(:product, taxons: [taxon], price: 20.00),
          create(:product, taxons: [taxon], price: 30.00),
        ]
      end

      it "is ordered by low price" do
        get :sort, params: { id: taxon.id, sort: "ascend_by_price" }
        expect(assigns(:products)).to match products
      end

      it "is ordered by high price" do
        get :sort, params: { id: taxon.id, sort: "descend_by_price" }
        expect(assigns(:products)).to match(products.reverse)
      end
    end

    context "order_by_available_on" do
      let!(:products) do
        [
          create(:product, taxons: [taxon], available_on: Date.current.in_time_zone),
          create(:product, taxons: [taxon], available_on: 1.day.ago),
          create(:product, taxons: [taxon], available_on: 2.days.ago),
        ]
      end

      it "is ordered by old product" do
        get :sort, params: { id: taxon.id, sort: "ascend_by_available" }
        expect(assigns(:products)).to match products.reverse
      end

      it "is ordered by new product" do
        get :sort, params: { id: taxon.id, sort: "descend_by_price" }
        expect(assigns(:products)).to match products
      end
    end
  end
end
