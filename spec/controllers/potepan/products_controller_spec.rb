require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#index" do
    let!(:products_new) do
      [
        create(:product, available_on: Date.current.in_time_zone),
        create(:product, available_on: 1.day.ago),
        create(:product, available_on: 2.days.ago),
        create(:product, available_on: 3.days.ago),
        create(:product, available_on: 4.days.ago),
      ]
    end

    before do
      get :index
    end

    it "resoponds successfully" do
      expect(response).to be_successful
    end

    it "render index" do
      expect(response).to render_template :index
    end

    it "has correct products_new" do
      expect(assigns(:products_new)).to match products_new
    end
  end

  describe "#show" do
    let!(:taxonomy_1) { create(:taxonomy, name: "Categories") }
    let!(:taxonomy_2) { create(:taxonomy, name: "Brand") }
    let!(:taxon_1) { create(:taxon, name: "Bags", taxonomy: taxonomy_1) }
    let!(:taxon_2) { create(:taxon, name: "Ruby on Rails", taxonomy: taxonomy_2) }
    let!(:product) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon_1, taxon_2]) }
    let!(:related_products_1) { create_list(:product, 2, taxons: [taxon_1]) }
    let!(:related_products_2) { create_list(:product, 2, taxons: [taxon_2]) }

    before do
      get :show, params: { id: product.id }
    end

    it "resoponds successfully" do
      expect(response).to be_successful
    end

    it "render show" do
      expect(response).to render_template :show
    end

    it "has correct product" do
      expect(assigns(:product)).to eq product
    end

    it "has correct related_products" do
      related_products = related_products_1 + related_products_2
      expect(assigns(:related_products)).to match_array related_products
    end
  end
end
