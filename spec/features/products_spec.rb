require 'rails_helper'

RSpec.feature "Products", type: :feature do
  # 2種類のtaxonomyを持つ
  given!(:taxonomy_1) { create :taxonomy, name: "Category" }
  given!(:taxonomy_2) { create :taxonomy, name: "Brand" }
  # Categoryは１つ、Brandは２つのtaxonを持つ
  given!(:taxon_1) { create :taxon, name: "Bags", taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }
  given!(:taxon_2) { create :taxon, name: "Ruby on Rails", taxonomy: taxonomy_2, parent_id: taxonomy_2.root.id }
  given!(:taxon_3) { create :taxon, name: "Ruby", taxonomy: taxonomy_2, parent_id: taxonomy_2.root.id }
  # BagsとRuby on Railsの両カテゴリーに属する商品を定義する
  given!(:product_main) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon_1, taxon_2]) }
  # 対象テスト用に別カテゴリーに属する商品を定義する
  given!(:product_sub) { create(:product, name: "Ruby T-shirts", taxons: [taxon_3]) }
  # taxon_1,taxon_2はそれぞれ2つの商品を持つ。つまり、product_mainは４つの関連商品を持つ
  given!(:related_product_1) { create(:product, name: "Bag 1", taxons: [taxon_1]) }
  given!(:related_product_2) { create(:product, name: "Bag 2", taxons: [taxon_1]) }
  given!(:related_product_3) { create(:product, name: "Ruby on rails 1", taxons: [taxon_2]) }
  given!(:related_product_4) { create(:product, name: "Ruby on rails 2", taxons: [taxon_2]) }

  before do
    visit potepan_product_path(product_main.id)
  end

  scenario "shows the product page successfully" do
    expect(page).to have_title "#{product_main.name} | BIG BAG"
    expect(page).to have_selector "h2", text: product_main.name
  end

  scenario "shows all products under Bags and Ruby on Rails category" do
    expect(page).to have_content(related_product_1.name)
    expect(page).to have_content(related_product_2.name)
    expect(page).to have_content(related_product_3.name)
    expect(page).to have_content(related_product_4.name)
  end

  scenario "render the each products page when click product name in related area" do
    within ".productsContent" do
      click_on related_product_1.name
    end
    expect(page).to have_title "#{related_product_1.name} | BIG BAG"
    expect(page).to have_selector "h2", text: related_product_1.name

    visit potepan_product_path(product_main.id)

    within ".productsContent" do
      click_on related_product_3.name
    end
    expect(page).to have_title "#{related_product_3.name} | BIG BAG"
    expect(page).to have_selector "h2", text: related_product_3.name
  end

  scenario "don't show unrelated products in related area" do
    expect(page).not_to have_content(product_sub.name)
  end

  scenario "don't show myself in related area" do
    within ".productsContent" do
      expect(page).not_to have_content(product_main.name)
    end
  end
end
