require 'rails_helper'

RSpec.feature "ProductsNews", type: :feature do
  given!(:product_old) { create(:product, available_on: 1.week.ago) }
  given!(:product_new) { create(:product, available_on: Date.current.in_time_zone) }
  given!(:products_new) { create_list(:product, 4, available_on: 1.day.ago) }

  before do
    visit potepan_products_path
  end

  scenario "shows the Top Page successfully" do
    expect(page).to have_title "Top Page | BIG BAG"
  end

  scenario "shows the new products" do
    expect(page).to have_content product_new.name
    expect(page).not_to have_content product_old.name
  end

  scenario "moves to the products show page when click" do
    click_on product_new.name
    expect(page).to have_title product_new.name
    expect(page).to have_selector "h2", text: product_new.name
  end
end
