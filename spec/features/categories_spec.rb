require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  # 2種類のtaxonomyを持つ
  given!(:taxonomy_1) { create :taxonomy, name: "Category" }
  given!(:taxonomy_2) { create :taxonomy, name: "Brand" }
  # Categoryは４つのtaxonを持つ
  given!(:taxon_1) { create :taxon, name: "Bags", taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }
  given!(:taxon_2) { create :taxon, name: "Clothing", taxonomy: taxonomy_1, parent_id: taxonomy_1.root.id }
  # taxon_2は子カテゴリーtaxon_3, taxon_4を持つ
  given!(:taxon_3) { create :taxon, name: "Shirts", taxonomy: taxonomy_1, parent_id: taxon_2.id }
  given!(:taxon_4) { create :taxon, name: "T-shirts", taxonomy: taxonomy_1, parent_id: taxon_2.id }
  # taxon_2以外のカテゴリーはそれぞれ商品を１つずつ持つ
  given!(:product_1) { create(:product, name: "bag", taxons: [taxon_1]) }
  given!(:product_2) { create(:product, name: "shirts_1", taxons: [taxon_3]) }
  given!(:product_3) { create(:product, name: "shirts_2", taxons: [taxon_4]) }

  before do
    visit potepan_category_path(taxon_1.id)
  end

  scenario "shows the Bags category page successfully" do
    expect(page).to have_title "#{taxon_1.name} | BIG BAG"
    expect(page).to have_selector "h2", text: taxon_1.name
  end

  scenario "shows all products under Bags category" do
    taxon_1.all_products.each do |bags_product|
      expect(page).to have_content(bags_product.name)
    end
  end

  scenario "moves to products_show when click on each product" do
    taxon_1.all_products.each do |bags_product|
      click_on(bags_product.name)
      expect(page).to have_selector "h2", text: bags_product.name
    end
  end

  scenario "shows 2 kinds of taxonomies" do
    expect(page).to have_content(taxonomy_1.name)
    expect(page).to have_content(taxonomy_2.name)
  end

  scenario "only leaf taxons are displayed" do
    within '.side-nav' do
      expect(page).to have_content(taxon_1.name)
      expect(page).to have_content(taxon_3.name)
      expect(page).to have_content(taxon_4.name)
      expect(page).not_to have_content(taxon_2.name)
    end
  end

  scenario "move to Shirts page by click_on link" do
    click_on(taxonomy_1.name)
    within '.side-nav' do
      click_on(taxon_3.name)
    end
    expect(page).to have_title "#{taxon_3.name} | BIG BAG"
    expect(page).to have_selector "h2", text: taxon_3.name
    taxon_3.products.each do |shirts_product|
      expect(page).to have_content(shirts_product.name)
    end
    taxon_1.products.each do |bags_product|
      expect(page).not_to have_content(bags_product.name)
    end
  end

  scenario "moves to the Clothing page by input URL directly" do
    visit potepan_category_path(taxon_2.id)
    expect(page).to have_title "#{taxon_2.name} | BIG BAG"
    expect(page).to have_selector "h2", text: taxon_2.name
    taxon_3.products.each do |shirts_product|
      expect(page).to have_content(shirts_product.name)
    end
    taxon_4.products.each do |t_shirts_product|
      expect(page).to have_content(t_shirts_product.name)
    end
    taxon_1.products.each do |bags_product|
      expect(page).not_to have_content(bags_product.name)
    end
  end
end
