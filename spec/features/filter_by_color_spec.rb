require 'rails_helper'

RSpec.feature "FilterByColors", type: :feature do
  # 用意するサンプル
  # Bagsカテゴリー
  given!(:taxon) { create :taxon, name: "Bags" }

  given!(:option_type) { create(:option_type, presentation: "Color") }

  given!(:option_value_1) { create(:option_value, presentation: "Yellow", option_type: option_type) }
  given!(:option_value_2) { create(:option_value, presentation: "White", option_type: option_type) }

  given!(:variant_1) { create(:variant, option_values: [option_value_1]) }
  given!(:variant_2) { create(:variant, option_values: [option_value_2]) }

  given!(:product_1) { create(:product, name: "yellow-bag", taxons: [taxon], variants: [variant_1]) }
  given!(:product_2) { create(:product, name: "white-bag", taxons: [taxon], variants: [variant_2]) }

  before { visit potepan_category_path(taxon.id) }

  # 色一覧が表示されていること
  scenario "shows the 2 colors in filter_by_color" do
    within '.filter_by_color' do
      expect(page).to have_content option_value_1.presentation
      expect(page).to have_content option_value_2.presentation
    end
  end

  scenario "filer white-bag when click on Yellow" do
    within '.filter_by_color' do
      click_on option_value_1.presentation
    end
    expect(page).to have_content product_1.name
    expect(page).not_to have_content product_2.name
  end

  scenario "moves to the yellow-bag page after click" do
    within '.filter_by_color' do
      click_on option_value_1.presentation
    end
    click_on product_1.name
    expect(page).to have_title "#{product_1.name} | BIG BAG"
    expect(page).to have_selector "h2", text: product_1.name
  end
end
