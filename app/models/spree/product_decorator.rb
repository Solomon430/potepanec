Spree::Product.class_eval do
  scope :with_price_and_images, -> {
    includes(master: [:default_price, :images])
  }
  scope :without_myself, -> (product) {
    where.not(id: product.id)
  }
  scope :related_products, -> (product, max) {
    in_taxons(product.taxons).distinct.sample(max)
  }
  scope :descend_by_available, -> {
    order(available_on: "DESC")
  }
  scope :ascend_by_available, -> {
    order(available_on: "ASC")
  }
  scope :select_by_category, -> (taxon) {
    taxon.all_products.with_price_and_images
  }
  scope :filter_by_color, -> (color) {
    joins(variants_including_master: :option_values).
      includes(variants_including_master: :option_values).
      where("spree_option_values.presentation = ?", color)
  }
  scope :filter_by_size, -> (size) {
    joins(variants_including_master: :option_values).
      includes(variants_including_master: :option_values).
      where("spree_option_values.presentation = ?", size)
  }
  scope :sort_by_order, -> (standard) {
    case standard
    when "ascend_by_price"
      ascend_by_master_price
    when "descend_by_price"
      descend_by_master_price
    when "descend_by_available"
      descend_by_available
    when "ascend_by_available"
      ascend_by_available
    end
  }

  scope :search_name_or_description, -> (search_word) {
    where(['name LIKE ? OR description LIKE ?', "%#{sanitize_sql_like(search_word)}%", 
      "%#{sanitize_sql_like(search_word)}%"]) if search_word.present?
  }
end
