Spree::OptionValue.class_eval do
  scope :presentation, -> (presentation) {
    includes(:option_type).
      where(spree_option_types: { presentation: presentation }).
      uniq
  }
end
