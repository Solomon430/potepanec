class Potepan::CategoriesController < ApplicationController
  helper_method :count_number_of_products
  before_action :setup_values

  def setup_values
    @taxonomies = Spree::Taxonomy.all
    @taxon = Spree::Taxon.find(params[:id])
    @colors = Spree::OptionValue.presentation("Color")
    @sizes = Spree::OptionValue.presentation("Size")
  end

  def show
    @products = Spree::Product.select_by_category(@taxon)
  end

  def color
    @products = Spree::Product.
      select_by_category(@taxon).
      filter_by_color(params[:color])
    render 'potepan/categories/show'
  end

  def size
    @products = Spree::Product.
      select_by_category(@taxon).
      filter_by_size(params[:size])
    render 'potepan/categories/show'
  end

  def sort
    @products = Spree::Product.
      select_by_category(@taxon).
      sort_by_order(params[:sort])
    render 'potepan/categories/show'
  end

  private

  def count_number_of_products(option_value)
    Spree::Product.
      includes(variants: :option_values).
      in_taxon(@taxon).
      where(spree_option_values: { name: option_value }).
      count
  end
end
