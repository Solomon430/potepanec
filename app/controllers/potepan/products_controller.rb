class Potepan::ProductsController < ApplicationController
  def index
    @products_new = Spree::Product.
      with_price_and_images.
      descend_by_available.
      limit(5)
  end

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.
      with_price_and_images.
      without_myself(@product).
      related_products(@product, 4)
  end

  def search
    @search_word = params[:search]
    @products = Spree::Product.
      with_price_and_images.
      search_name_or_description(@search_word)
  end
end
